import { fetchJimmsGPUs } from "./jimmsCrawler";
import { fetchVKGPUs } from "./vkCrawler";
import { GPUInfo, GPUModel } from "./utils";

import express from "express";
const app = express();

let jimmsGPUs: Array<GPUInfo> = [];
let vkGPUs: Array<GPUInfo> = [];
let allGPUs: Array<GPUInfo> = [];

interface GPURequirement {
  maxPrice: number;
  model: GPUModel;
}

const requirements: Array<GPURequirement> = [
  { maxPrice: 1400, model: GPUModel.AMD6800XT },
  { maxPrice: 1400, model: GPUModel.AMD6800 },
  { maxPrice: 1900, model: GPUModel.AMD6900 },
  { maxPrice: 1300, model: GPUModel.NV3080TI },
  { maxPrice: 2000, model: GPUModel.NV3090 },
];

app.get("/available/:maxPrice", (req, res) => {
  try {
    const maxPrice = req.params.maxPrice;
    if (maxPrice) {
      const gpus = getAvailableGPUS(maxPrice);
      res.json(gpus);
    } else {
      res.send("Required parameters missing.");
    }
  } catch (err) {
    console.log(err);
    res.json("No match");
  }
});

app.get("/available/:maxPrice/:brand", (req, res) => {
  try {
    const brand = req.params.brand;
    const maxPrice = req.params.maxPrice;
    if (brand && maxPrice) {
      const gpus = getAvailableGPUS(maxPrice, brand);
      res.json(gpus);
    } else {
      res.send("Required parameters missing.");
    }
  } catch (err) {
    console.log(err);
    res.json("No match");
  }
});

app.get("/", (req, res) => {
  res.json("Welcome");
});

app.get("/available", (req, res) => {
  const available = getAvailableGPUS("10000");
  res.json(available);
});

app.get("/all", (req, res) => {
  res.json(allGPUs);
});

app.listen(3000, () => {
  updateGPUList();
  setInterval(async () => {
    updateGPUList();
  }, 60000);
});

const updateGPUList = async () => {
  //Empty the array.
  allGPUs.splice(0, allGPUs.length);
  jimmsGPUs = await fetchJimmsGPUs();
  vkGPUs = await fetchVKGPUs();
  allGPUs = allGPUs.concat(jimmsGPUs, vkGPUs);
  logIfRequirementsMet();
};

const logIfRequirementsMet = () => {
  let found = false;
  let availableGPUs: Array<GPUInfo> = [];
  allGPUs.forEach((gpu) => {
    requirements.forEach((requirement) => {
      if (
        gpu.price < requirement.maxPrice &&
        gpu.model === requirement.model &&
        gpu.available
      ) {
        availableGPUs.push(gpu);
        found = true;
      }
    });
  });
  if (found) {
    console.log(availableGPUs);
  } else {
    console.log(`Requirements not met at ${new Date()}`);
  }
};

const getCorrectArray = (brandName?: string) => {
  const name = brandName?.toLowerCase();
  switch (name) {
    //case 'amd': return allGPUs.filter((val) => val.brand === 'amd');
    //case 'nvidia': return allGPUs.filter((val) => val.brand === 'nvidia');
    default:
      return allGPUs;
  }
};

const getAvailableGPUS = (maxPrice: string, brand?: string) => {
  let sourceList = getCorrectArray(brand);
  try {
    const price = parseInt(maxPrice);
    return sourceList.filter((val, index) => {
      return val.price < price && val.available;
    });
  } catch (err) {
    console.log(err);
    return [];
  }
};
