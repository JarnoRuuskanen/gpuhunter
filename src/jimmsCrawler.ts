import { fetchHTML, GPUInfo, GPUModel, UrlAndModel } from "./utils";

const jimmsUrls: Array<UrlAndModel> = [
  {
    url: "https://www.jimms.fi/fi/Product/List/000-1T4/komponentit--naytonohjaimet--amd-radeon--rx-6000-sarja--rx-6800-xt?i=100&ob=6",
    model: GPUModel.AMD6800XT,
  },
  {
    url: "https://www.jimms.fi/fi/Product/List/000-1T5/komponentit--naytonohjaimet--amd-radeon--rx-6000-sarja--rx-6900-xt",
    model: GPUModel.AMD6900,
  },
  {
    url: "https://www.jimms.fi/fi/Product/List/000-1U3/komponentit--naytonohjaimet--geforce-rtx-pelaamiseen--rtx-3080-ti?i=100&ob=6",
    model: GPUModel.NV3080TI,
  },
  {
    url: "https://www.jimms.fi/fi/Product/List/000-1T0/komponentit--naytonohjaimet--geforce-rtx-pelaamiseen--rtx-3090",
    model: GPUModel.NV3090,
  },
];

export const fetchJimmsGPUs = async (): Promise<Array<GPUInfo>> => {
  let gpus: Array<GPUInfo> = [];
  for (const item of jimmsUrls) {
    const urlGPUs = await fetchJimmsFromUrl(item.url, item.model);
    gpus = gpus.concat(urlGPUs);
  }
  return gpus;
};

const fetchJimmsFromUrl = async (url: string, model: GPUModel) => {
  try {
    const $ = await fetchHTML(url);
    let re = new RegExp("ProductListV2ViewModel(.*);");
    let list = re.exec($.html());
    if (list) {
      const listString = list.toString();
      const firstIndex = listString.indexOf("{");
      const lastIndex = listString.indexOf(";");
      const productsJson = listString.substring(firstIndex, lastIndex - 4);
      const listJSON = JSON.parse(productsJson);
      return getGPUS(listJSON.Products, model);
    }
  } catch (err) {
    console.log(err);
  }
  return [];
};

interface JimmsGPU {
  VendorName: string;
  Name: string;
  PriceTax: number;
  Uri: string;
  DeliveryInfoText: string;
}

const getGPUS = (gpus: Array<JimmsGPU>, model: GPUModel) => {
  let available: Array<GPUInfo> = [];
  gpus.map((val) => {
    available.push({
      name: `${val.VendorName} ${val.Name}`,
      price: val.PriceTax,
      shopName: "Jimms",
      url: "https://www.jimms.fi/fi/Product/Show/" + val.Uri,
      info: val.DeliveryInfoText,
      available:
        val.DeliveryInfoText !== null &&
        val.DeliveryInfoText.indexOf("Varastossa") !== -1,
      model,
    });
  });
  return available;
};
