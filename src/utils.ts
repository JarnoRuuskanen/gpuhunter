import axios from "axios";
import cheerio from "cheerio";

export enum GPUModel {
  AMD6700,
  AMD6800,
  AMD6800XT,
  AMD6900,
  NV3080,
  NV3080TI,
  NV3090,
}

export interface UrlAndModel {
  url: string;
  model: GPUModel;
}

export interface GPUInfo {
  name: string;
  price: number;
  shopName: string;
  url: string;
  info: string;
  available: boolean;
  model: GPUModel;
}

export const fetchHTML = async (url: string) => {
  const { data } = await axios.get(url);
  return cheerio.load(data);
};
