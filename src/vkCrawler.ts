import { fetchHTML, GPUInfo, GPUModel, UrlAndModel } from "./utils";
import puppeteer, { Page } from "puppeteer";
import cheerio from "cheerio";

const vkUrls: Array<UrlAndModel> = [
  {
    url: "https://www.verkkokauppa.com/fi/catalog/12147c/AMD-Radeon-RX-6800",
    model: GPUModel.AMD6800XT,
  },
  {
    url: "https://www.verkkokauppa.com/fi/catalog/12148c/AMD-Radeon-RX-6900",
    model: GPUModel.AMD6900,
  },
  {
    url: "https://www.verkkokauppa.com/fi/catalog/12069c/NVIDIA-GeForce-RTX-3080",
    model: GPUModel.NV3080TI,
  },
  {
    url: "https://www.verkkokauppa.com/fi/catalog/12071c/NVIDIA-GeForce-RTX-3090",
    model: GPUModel.NV3090,
  },
];

export const fetchVKGPUs = async (): Promise<Array<GPUInfo>> => {
  const browser = await puppeteer.launch({
    executablePath: "/usr/bin/chromium",
  });
  const page = await browser.newPage();
  await page.setUserAgent(
    "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.125 Safari/537.36"
  );
  let gpus: Array<GPUInfo> = [];
  for (const item of vkUrls) {
    const urlGPUs = await fetchGPUInfo(page, item.url, item.model);
    gpus = gpus.concat(urlGPUs);
  }
  page.close();
  browser.close();
  return gpus;
};

const fetchGPUInfo = async (
  page: puppeteer.Page,
  url: string,
  model: GPUModel
) => {
  await page.goto(url);
  await page.waitForTimeout(2000);
  const listContent = await page.evaluate(() => {
    const data = [];
    let elements = document.getElementsByClassName(
      "product-list-detailed__item"
    );
    for (const elem of elements) {
      data.push(elem.innerHTML);
    }
    return data;
  });
  let gpus = [];
  for (const item of listContent) {
    const $ = cheerio.load(item);
    const cartButton = $(".list-product-cart-buttons:first").find("button");
    const gpu = getGPUInfoFromDiv($, cartButton, model);
    gpus.push(gpu);
  }
  return gpus;
};

const getGPUInfoFromDiv = (
  $: cheerio.Root,
  cartButton: cheerio.Cheerio,
  model: GPUModel
): GPUInfo => {
  try {
    const available = !cartButton.attr("aria-disabled");
    const url =
      "https://www.verkkokauppa.com" + $("a:first").attr("href")?.toString();
    const priceString = $(".product-price__price")
      .text()
      .trim()
      .replace(/\s/g, "")
      .replace(/,/g, ".");
    const price = parseFloat(priceString);
    const name = $("a:first").text();
    const info = $(".list-product-info__description").text();
    return {
      name,
      price,
      shopName: "Verkkokauppa.com",
      url,
      info,
      available,
      model,
    };
  } catch (err) {
    console.log(err);
    return {
      name: "",
      price: 100000,
      shopName: "Verkkokauppa",
      url: "",
      info: "",
      available: false,
      model,
    };
  }
};
